package br.com.maposerv.cliente.conttroller;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import br.com.maposerv.cliente.exception.ResourceNotFoundException;
import br.com.maposerv.cliente.model.Cliente;
import br.com.maposerv.cliente.repository.ClienteRepository;

@RestController
@RequestMapping("/api_clientes")
public class ClienteController {
	
    @Autowired
    private ClienteRepository clienteRepository;

    
    @GetMapping("/clientes/{tipo}/{ini}/{num}")
    public Page<Cliente> getClientes(@PathVariable(value = "ini") int regIni, @PathVariable(value = "num") int totalReg) {
        PageRequest pageRequest = PageRequest.of(regIni, totalReg, Sort.Direction.ASC,"id");
        return new PageImpl<>(clienteRepository.findAll(), pageRequest, totalReg);
    }    
 
    
    @GetMapping("/clientes/{id}")
    public ResponseEntity <Cliente> getClienteById(@PathVariable(value = "id") Long clienteId)
    throws ResourceNotFoundException {
        Cliente cliente = clienteRepository.findById(clienteId)
            .orElseThrow(() -> new ResourceNotFoundException("Cliente deste ID nÃ£o encontrado! " + clienteId));
        return ResponseEntity.ok().body(cliente);
    }        
  
    
    @GetMapping("/clientes/{inf}/{val}")
    public ResponseEntity <Cliente> getClienteByInfVal(@PathVariable(value = "inf") String clienteInf, @PathVariable(value = "val") String clienteVal)
    throws ResourceNotFoundException {    
    	Cliente cliente = null;
    	switch(clienteInf){
			case "nome": cliente = clienteRepository.findByNome(clienteVal)
					.orElseThrow(() -> new ResourceNotFoundException("Cliente deste nome não encontrado! " + clienteVal)); break;
			case "cpf":  cliente = clienteRepository.findByCpf(Long.parseLong(clienteVal))
					.orElseThrow(() -> new ResourceNotFoundException("Cliente deste cpf não encontrado! " + clienteVal)); break;
			default: break; 
    	}	  	
    	return ResponseEntity.ok().body(cliente);
    }    
    
    
    @PostMapping("/clientes")
    public Cliente createCliente(@Valid @RequestBody Cliente cliente) {
        return clienteRepository.save(cliente);
    }    
   
    
    @PutMapping("/clientes/{id}")
    public ResponseEntity <Cliente> updateCliente(@PathVariable(value = "id") Long clienteId,
        @Valid @RequestBody Cliente clienteDetails) 
    throws ResourceNotFoundException {
        Cliente cliente = clienteRepository.findById(clienteId)
            .orElseThrow(() -> new ResourceNotFoundException("Cliente deste ID não encontrado! " + clienteId));

        cliente.setNome(clienteDetails.getNome());
        cliente.setCpf(clienteDetails.getCpf());
        cliente.setDataNasc(clienteDetails.getDataNasc());
        
        final Cliente updatedCliente = clienteRepository.save(cliente);
        return ResponseEntity.ok(updatedCliente);
    }    
   
    
    @DeleteMapping("/clientes/{id}")
    public Map < String, Boolean > deleteCliente(@PathVariable(value = "id") Long clienteId)
    throws ResourceNotFoundException {
        Cliente cliente = clienteRepository.findById(clienteId)
            .orElseThrow(() -> new ResourceNotFoundException("Cliente deste ID não encontrado! " + clienteId));

        clienteRepository.delete(cliente);
        Map < String, Boolean > response = new HashMap < > ();
        response.put("deleted", Boolean.TRUE);
        return response;
    }    

    @PatchMapping("/clientes/{id}/{inf}")
    public ResponseEntity <Cliente> changeCliente(@PathVariable(value = "id") Long clienteId,
    	@PathVariable(value = "inf") String clienteInf, @Valid @RequestBody Cliente clienteDetails) 
    throws ResourceNotFoundException { 
    	Cliente cliente = clienteRepository.findById(clienteId)
            .orElseThrow(() -> new ResourceNotFoundException("Cliente deste ID não encontrado! " + clienteId));

    	switch(clienteInf){
    		case "nome": cliente.setNome(clienteDetails.getNome()); break;
    		case "cpf": cliente.setCpf(clienteDetails.getCpf()); break;	
    		case "dataNasc": cliente.setDataNasc(clienteDetails.getDataNasc()); break;
    		case "idade": cliente.setIdade(clienteDetails.getIdade()); break;
    		default: break;
        }
    	
        final Cliente changeCliente = clienteRepository.save(cliente);
        return ResponseEntity.ok(changeCliente);
    }        
    
    
}
