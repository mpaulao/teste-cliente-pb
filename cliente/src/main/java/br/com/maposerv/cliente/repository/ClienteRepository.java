package br.com.maposerv.cliente.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import br.com.maposerv.cliente.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	
	@Query("select cl from Cliente cl where cl.cpf = ?1")
	Optional <Cliente> findByCpf(long cpf); 
	
	@Query("select cl from Cliente cl where cl.nome = ?1") 
	Optional <Cliente> findByNome(String nome);

}
