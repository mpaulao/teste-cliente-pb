package br.com.maposerv.cliente.model;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name = "LISTA_CLIENTES")
public class Cliente {
	
    private long id;
    private String nome;
    private long cpf;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy") 
    private LocalDate dataNasc; 
    private int idade;
    
    public Cliente() {

    }
    
    public Cliente(String nome, long cpf, LocalDate dataNasc, int idade) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNasc = dataNasc;
        this.idade = idade;
    }    
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }    
    
    @Column(name = "Nome_Cliente", nullable = false)
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    @Column(name = "Cpf_Cliente", nullable = false)
    public long getCpf() {
        return cpf;
    }
    public void setCpf(long cpf) {
        this.cpf = cpf;
    }
    
    @Column(name = "DataNasc_Cliente", nullable = false)
    public LocalDate getDataNasc() {
        return dataNasc;
    }
    public void setDataNasc(LocalDate dataNasc) {
        this.dataNasc = dataNasc;   
        setIdade(0);
    }
    
    
    @Column(name = "Idade_Cliente", nullable = false)
    public int getIdade() {
        return idade;
    }
    public void setIdade(int idade) {
    	Calendar cal = GregorianCalendar.getInstance();
    	if(this.dataNasc.getYear() <= cal.get(Calendar.YEAR)) {
    		this.idade = cal.get(Calendar.YEAR) - this.dataNasc.getYear();	
    	}else {
    		this.idade = 0;	
    	}
    }    
}
